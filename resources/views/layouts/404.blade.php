@extends('layouts.app')

@section('content')


<div class="container d-flex justify-content-center aligne-items-center">
    <h1>
        <strong>
            {{$status ?? '404'}} 
        </strong>
        {{$error}}
    </h1>
</div>
@endsection
@extends('layouts.app')

@section('content')
<div class="container">
    <form action="{{route('article.update', $article->id)}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="title">Article title</label>
            <input type="text" class="form-control" id="title" name="title" value="{{ $article->title }}">
        </div>
        <div class="form-group">
            <label for="description">Article short description</label>
            <textarea class="form-control" id="description" name="description" rows="3">{{ $article->description }}</textarea>
        </div>
        <div class="form-group">
            <label for="content">Article body</label>
            <textarea class="form-control" id="content" name="content" rows="15">{{ $article->content }}</textarea>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection
@extends('layouts.app')

@section('content')
<div class="container">

    <form action="{{route ('article.store')}}" method="POST">
        @csrf <!--Uurida mis see teeb-->
        <div class="form-group">
            <label for="title">Article title</label>
            <input type="text" class="form-control" id="title" name="title">
        </div>
        <div class="form-group">
            <label for="description">Article short description</label>
            <textarea class="form-control" id="description" name="description" rows="3"></textarea>
        </div>
        <div class="form-group">
            <label for="content">Article body</label>
            <textarea class="form-control" id="content" name="content" rows="15"></textarea>
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection
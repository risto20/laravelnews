@extends('layouts.app')

@section('content')
<div class="container">
    <h1> {{ $article->title }} </h1>
    <p class="text-muted"> {{ $article->description}}</p>
    <p>{{ $article->content }}</p>
    <a class="btn btn-secondary" href="{{ route('home') }}">Back</a>
</div>
@endsection
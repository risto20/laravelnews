<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    //table name has to be specified ONLY if table name is not model name in plurar(mitmuses)
    //protected $table = 'articles';
}

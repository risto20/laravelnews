<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Article;

class articleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   //get articles from database
        $articles = Article::all();
        $total = Article::all()->count();

        //pass articles to"articles all" view
        return view ('articles.all',['total' => $total, 'articles' => $articles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //siin kuvab resourcest articles create.blade
        return view('articles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this -> validate($request,[
            'title' => 'required|string|max:225|min:1',
            'description' => 'required|string|max:225|min:1',
            'content' => 'required|string|max:10000|min:1'

        ]);
        //create new instance of article from form data
        $article = new Article();
        //save data to new instance

        $article->title = $request->title;

        $article->description = $request->description;

        $article->content = $request->content;

        $article->author_id = Auth::id();
        //store data as a new row in database
        $article->save();
        return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //get the specified article from database
        $article = Article::find($id);
        //display the current article
       
        
        return view('articles.single')->with('article',$article);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //TODO: check if user has rights to edit this article
        //get the article from the database
        $article = Article::find($id);
        //show form for editing and fill it with data from database
        return view('articles.edit')->with('article', $article);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      //
      $article = Article::find($id);
      $article->title = $request->title;
      $article->description = $request->description;
      $article->content = $request->content;
      $article->save();
      return redirect()->route('article.show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $article = Article::find($id);
        //TODO: check if the article belong to the currently logged in user
        if($article->author_id == Auth::id()){ //kui inimene pole sisse logitud, siis == 0
            $article->delete();
            //ümbersuunamine nüüd:
            return redirect()->route('home');
        }else{
            return view('404')->with(['status' => '401', 'error' => 'Bad credentials' ]);//see tekitab errori

        }
    }
}

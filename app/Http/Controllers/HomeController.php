<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Article;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //return view('home');
        $ownedArticles = Article::where('author_id', Auth::id())->get();
        //return view('home',$ownedArticles);
        //varem sai ka nii:
        return view ('home')->with('articles',$ownedArticles);

    }
}

//controllereid uurida!!!